package com.trupti;

public abstract class TestAbstractClass {
 
	public abstract void testAbstract();
	
	public static void test() {
		System.out.println("I am concrete method of abstract class");
	}
}
