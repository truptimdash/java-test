package com.trupti;

public class ChildInterface implements TestInterface {
	public void testInterface() {
		System.out.println("I am a Interface method");
	}

	public static void main(String[] args) {
		TestInterface childInterface = new ChildInterface();
		childInterface.testInterface();
		childInterface.testMethod();
		TestInterface.testIMethod();
	}
}
