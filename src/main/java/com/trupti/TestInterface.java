package com.trupti;

public interface TestInterface {
	public void testInterface();

	public static void testIMethod() {
		System.out.println("Static method can access of an interface using its reference");
	}

	default void testMethod() {
		System.out.println("deafult method");
	}
}
