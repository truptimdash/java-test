package com.trupti;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class TestCollection {
//Given a sentence (For Example Hi Debashish, How are you, How is your day Debashish). Display the Occurrence (How many times a word present in the sentence) of each word
	public Map<String, Integer> getOccurrenceOfWord(String sentence) {
		String removeComma = sentence.replaceAll(",", "").trim();
		String[] wordArray = removeComma.split(" ");
		Map<String, Integer> countMap = new HashMap<String, Integer>();
		for (String word : wordArray) {
			if (countMap.containsKey(word)) {
				countMap.put(word, countMap.get(word) + 1);
			} else {
				countMap.put(word, 1);
			}
		}
		return countMap;
	}

//Given 2 Array List find a list of common values present in both the list.
	public List<String> getCommonValue(List<String> list, List<String> list1) {
		List<String> commonValue = new ArrayList<String>(list);
		commonValue.retainAll(list1);
		return commonValue;

	}

	public List<String> getCommonValue1(List<String> list, List<String> list1) {
		List<String> commonValue = new ArrayList<String>();
		for (String string : list) {
			for (String string2 : list1) {
				if (string.equals(string2)) {
					commonValue.add(string);
				}
			}

		}
		return commonValue;

	}

//Given 2 different List, combine both the list and remove all the duplicate values from the list.
	public List<String> getRemoveAllDuplicateValue(List<String> list, List<String> list1) {
		List<String> removeDuplicate = new ArrayList<String>();
		removeDuplicate.addAll(list);
		for (String string1 : list1) {
			if (!removeDuplicate.contains(string1)) {
				removeDuplicate.add(string1);

			}
		}

		return removeDuplicate;

	}

	public Set<String> getRemoveAllDuplicateValue1(List<String> list, List<String> list1) {
		Set<String> removeDuplicate = new HashSet<String>(list);
		removeDuplicate.addAll(list1);
		return removeDuplicate;

	}

	// Give a List with integer values in the list, find the total sum of the
	// values.
	public Integer getSumOFValues(List<Integer> list) {
		int sum = 0;
		for (int i = 0; i < list.size(); i++) {
			sum = sum + list.get(i);

		}
		return sum;
	}

//Give a a List with integer values in the list, find largest number in the list
	public Integer getLargestValue(List<Integer> list) {
		int largeNum = Integer.MIN_VALUE;
		for (int i = 0; i < list.size(); i++) {
			if (largeNum < list.get(i)) {
				largeNum = list.get(i);
			}
		}
		return largeNum;

	}

	public Integer getLargestValue1(List<Integer> list) {
		Collections.sort(list);
		int largeNum = list.get(list.size() - 1);

		return largeNum;

	}

	public Integer getSmallestValue(List<Integer> list) {
		int smallNum = Integer.MAX_VALUE;
		for (int i = 0; i < list.size(); i++) {
			if (smallNum > list.get(i)) {
				smallNum = list.get(i);
			}
		}
		return smallNum;

	}

	public Integer getSmallestValue1(List<Integer> list) {
		Collections.sort(list);
		int smallNum = list.get(0);

		return smallNum;

	}

	// Given a list with String, find the count of records (Duplicate values will be
	// Counted as One).
	public List<String> getCountRecord(List<String> list) {
		List<String> listOfRecords = new ArrayList<String>();
		for (int i = 0; i < list.size(); i++) {
			for (int j = 0; j < list.size(); j++) {
				if (list.get(i).equals(list.get(j))) {
					if (!listOfRecords.contains(list.get(i))) {
						listOfRecords.add(list.get(i));
					}
				}
			}
		}
		return listOfRecords;
	}

	public Set<String> getDuplicateOne(List<String> list) {
		Set<String> duplicateValueTakeOnces = new HashSet<String>();
		duplicateValueTakeOnces.addAll(list);
		return duplicateValueTakeOnces;
	}

	public Set<String> getSortedValue(List<String> list) {
		Set<String> sorted = new TreeSet<String>();
		sorted.addAll(list);
		return sorted;
	}

	// For example if List has {"Debashish, "Trupti", "Debashish", "Devesh"}, total
	// count should show 3.
	public Integer getCountRecord1(List<String> list) {
		List<String> listOfRecords = new ArrayList<String>();
		int count = 0;
		for (int i = 0; i < list.size(); i++) {
			for (int j = 0; j < list.size(); j++) {
				if (list.get(i).equals(list.get(j))) {
					if (!listOfRecords.contains(list.get(i))) {
						listOfRecords.add(list.get(i));
						count++;
					}
				}
			}
		}
		return count;
	}

	public Map<String, String> getStringInOrder() {
		Map<String, String> map = new TreeMap<String, String>();
		map.put("Trupti", "Dash");
		map.put("Debashish", "Dash");
		map.put("Mamunu", "Pani");
		return map;
	}

	// Create a method which will take list for String as input and sort them in
	// alphabetical order asc,dsc
	public List<String> getAlphabeticOrder(List<String> list) {
		List<String> ascOrder = new ArrayList<String>(list);
		Collections.sort(ascOrder);// asc
		Collections.sort(ascOrder, Collections.reverseOrder());
		return ascOrder;

	}

	// Create a method which will take list for String as input and sort them in
	// alphabetical order desc
	public List<String> getDescOrderString(List<String> list) {
		Collections.sort(list, Collections.reverseOrder());
		return list;
	}

	// Create a method which will take list for String as input and sort them in
	// alphabetical order asc
	public List<String> getAscOrderString(List<String> list) {
		Collections.sort(list);
		return list;
	}

	public static void main(String[] args) {
		TestCollection collection = new TestCollection();
		Map<String, Integer> map = collection
				.getOccurrenceOfWord("Hi Debashish, How are you, How is your day Debashish");
		for (String string : map.keySet()) {
			// System.out.println(string + ":" + map.get(string));
		}
		//System.out.println("String in order:" + collection.getStringInOrder());
		List<String> list = new ArrayList<String>();
		list.add("Trupti");
		list.add("Mamunu");
		list.add("Devesh");
		list.add("Debashish");
		List<String> list1 = new ArrayList<String>();
		list1.add("Devansh");
		list1.add("Mamunu");
		list1.add("Devesh");
		System.out.println(collection.getCommonValue(list, list1));
		List<Integer> listAdd = new ArrayList<Integer>();
		listAdd.add(10);
		listAdd.add(-15);
		listAdd.add(-1);
		// System.out.println("Sum of value is:" +
		// collection.getSmallestValue1(listAdd));
		List<String> duplicateCount = new ArrayList<String>();
		duplicateCount.add("Mamunu");
		duplicateCount.add("Mamunu");
		duplicateCount.add("Debashish");
		duplicateCount.add("Debashish");
		duplicateCount.add("Devesh");
		// System.out.println(collection.getCountRecord(duplicateCount));
	}
}
