package com.trupti;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TestString {

	// Test String is Immutable
	public void getStringIsImmutable(String name) {
		name.concat("Dash");
		System.out.println("String is Immutable:" + name);

	}

	// If a string more than 15 character then it is a large string.
	public boolean isStringIsLarger(String name) {
		return name.length()>15;

	}

	// Compare both these type of string as Same. "Hello Debashish" "How are you
	// Debashish". Debashish could be replaced as Trupti, Devesh or any string

	public boolean isStringSame(String name, String name1) {
		String value = name.replaceFirst("Hello", "").trim();
		String value1 = name1.replaceFirst("How are you", "").trim();
		return value.equalsIgnoreCase(value1);

	}
	public boolean isStringSame1(String name, String name1) {
		String value = name.substring(6,15).trim();
		String value1 = name1.substring(11, 21).trim();
		System.out.println(value1);
		return value.equalsIgnoreCase(value);

	}

	// Find the Extn of a file.Ex- "abc.txt"
	public String getFileExtension(String string) {
		if (string.lastIndexOf(".")== -1) {
			System.out.println("file not exit");
		}
		return	string.substring(string.lastIndexOf("."));
		
	}

	// Show the number of charcter before comma(,)
	public String getCharBeforeComma(String string) {
		String str = string.substring(0,string.lastIndexOf(","));
		System.out.println(str);
		return str;
	}

//Compare both these string are same. "Trupti"  "1.Trupti"
	public boolean isValueSame(String name, String name1) {
		String value = name1.replaceAll("1.", "");
		return name.equalsIgnoreCase(value);
	}

	// Compare these values as same "1012345.28" "1,012,345.28"
	public boolean isValueIsSame(String value,String value1) {
		String val = value1.replaceAll(",", "");
		double d1 = Double.parseDouble(val);
		double d2 = Double.parseDouble(value);
		System.out.println(d1);
		return d1 == d2;
	}
// Reverse the String
		public StringBuilder getSting(String list) {
		StringBuilder builder = new StringBuilder(list);
		return builder.reverse();
}
	public boolean isStringPresent(String name,String name1) {
		return name1.contains(name);
		
	}
	
	// Create a Date Object for current date

	public static void main(String[] args) {
		TestString testString = new TestString();
		// testString.getStringIsImmutable("Trupti");
		 //System.out.println(testString.isStringIsLarger("I m naughty"));
	 //System.out.println(testString.isStringSame1("Hello Debashish", "How are you Debashish"));
		// testString.compareTwoString1("Hello Debashish", "How are you Debashish");
		// testString.getSameString("Trupti", "1.Trupti");
		//System.out.println(testString.isValueSame("Trupti", "1.Trupti"));
		System.out.println(testString.isValueIsSame("1012345.28","1,012,345.28"));
		// System.out.println(testString.isValuesAreSame1("1012345.28",
		// "1,012,345.28"));
		// System.out.println(testString.isValuesAreSam("£123.00", "123"));
		// System.out.println(testString.isValuesAreSame("£2123.00", "2,123.00"));
	//System.out.println(testString.getFileExtension("abc.txt"));
		// testString.testConcat("Java", "Programming");
		// testString.testEqualMethod();
	testString.getCharBeforeComma("Java,programming,I am");
		//System.out.println(testString.getOrderString());
		List<String> list = new ArrayList<String>();
		list.add("Trupti");
		list.add("Bina");
		list.add("Devesh");
		list.add("Sunu");
		//System.out.println(testString.getDescOrderString(list));
		// testString.currentDate();

	}
}
