package com.trupti;

public class ChildAbstractClass extends TestAbstractClass {

	public void testAbstract() {
		System.out.println("I am abstract method");
	}

	public static void main(String[] args) {
		TestAbstractClass childAbstractClass = new ChildAbstractClass();
		//TestAbstractClass childAbstractClass = new TestAbstractClass(); Can not Instatiet the only Abstract class
		test();
		childAbstractClass.testAbstract();
	}
}