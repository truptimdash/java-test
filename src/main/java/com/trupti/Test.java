package com.trupti;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class Test {

	// Given a sentence (For Example Hi Debashish, How are you, How is your day
	// Debashish). Display the Occurrence (How many times a word present in the
	// sentence) of each word
	public Map<String, Integer> getOccrrenceOfWord(String sentence) {
		String string = sentence.replaceAll(",", "");
		String[] wordArray = string.split(" ");
		Map<String, Integer> countMap = new HashMap<String, Integer>();
		for (String word : wordArray) {
			if (countMap.containsKey(word)) {
				countMap.put(word, countMap.get(word) + 1);
			} else {
				countMap.put(word, 1);
			}
		}
		return countMap;
	}

	// Given 2 Array List find a list of common values present in both the list

	// Given 2 different List, combine both the list and remove all the duplicate
	// values from the list.

	// Give a List with integer values in the list, find the total sum of the
	// values.

	// Give a a List with integer values in the list, find largest number in the
	// list

	// Given a list with String, find the count of records (Duplicate values will be
	// Counted as One).

	// For example if List has {"Debashish, "Trupti", "Debashish", "Devesh"}, total
	// count should show 3

	// Test String is Immutable

	// If a string more than 15 character then it is a large string.
	// Compare both these type of string as Same. "Hello Debashish" "How are you
	// Debashish". Debashish could be replaced as Trupti, Devesh or any string

	// Show the number of charcter before comma(,)

	// Compare both these string are same. "Trupti" "1.Trupti"

	// Compare these values as same "1012345.28" "1,012,345.28"

	// Reverse the String

// Find the Extn of a file.Ex- "abc.txt"

	public void searchString() {
		String[] myArray = { "bread", "milk", "sugar", "coffee" };
		for (int i = 0; i < myArray.length; i++) {
			if (myArray[i].equals("milk")) {
				System.out.println(myArray[i]);
			}
		}
	}

	public static void main(String[] args) {
		Test test = new Test();

		Map<String, Integer> map = test.getOccrrenceOfWord("Hi Debashish, How are you, How is your day Debashish");
		for (String string : map.keySet()) {
			System.out.println(string + ":" + map.get(string));
		}

		List<String> list = new ArrayList<String>();
		list.add("Trupti");
		list.add("Sunu");
		list.add("Mamunu");
		list.add("Devesh");
		// list.add("Debashish");
		// list.add("Mamunu");
		// list.add("Devesh");
		//System.out.println(test.searchStrings(list));
		// System.out.println("The total list count is:"+test.getDuplicate(list));
		// System.out.println("The sorted list is:"+test.getSortedValue(list));
		List<String> list1 = new ArrayList<String>();
		list1.add("Devansh");
		list1.add("Mamunu");
		list1.add("Devesh");
		// System.out.println(test.getListOfCommonValue(list, list1));
		// System.out.println("Remove Duplicate Value:" +
		// test.getRemoveDuplicateValue1(list, list1));
		List<Integer> list2 = new ArrayList<Integer>();
		list2.add(100);
		list2.add(32);
		// System.out.println("Total value is:"+test.getSumOfValue(list2));
		// System.out.println("LargetValue is:"+test.getLargestValue1(list2));
		// System.out.println("Small value is:"+test.getSmallValue1(list2));
		// System.out.println("String is:"+test.isStringImmutable("Trupti"));
		// System.out.println("Check String length:"+ test.IsStringIsLarger("I am a
		// naughty girl"));
		// System.out.println("Comapre value is:" + test.isStringIsSame1("Hello
		// Debashish", "How are you debashish"));
		// System.out.println("Number of character is:" + test.getNumberOfChar("Hello
		// Debashish,How are you debashish"));
		// System.out.println("Number of character is:"
		// +test.isCompareValueIsSame("Trupti","1.Trupti"));

		// System.out.println("Number are same:"
		// +test.isNumbersAreSame("1012345.28","1,012,345.28"));
		// System.out.println("Reverse String is:"+test.getReverseString("Sunu My
		// Love"));
		// System.out.println("File Extn is:"+test.getFileExtn("File.txt"));
	}
}